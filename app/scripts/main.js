$(document).ready(function () {

  $('.i-cases').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.i-companies').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.i-feedback__cards').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.i-nav').click(function () {
    $('.i-mob-wrapper').slideToggle('fast', function () {
    });
  });

  new WOW().init();

});